#include <iostream>
#include <string>
using namespace std;

void search(string ti[], string di[], string pr[], string ac[], string av[], string bu[]);
void searchindex(string ttl[], string key2);
int index = 25;
int x;

int main()
{
	string title[25] = {"ABeautifulMind(2001)", "AntMan(2015)", "AntManandtheWasp(2018)", "Apocalypto(2006)", 
		"AvengersAgeofUltron(2015)", "AvengersInfinityWar(2018)", "BabyDriver (2017)", "BeforeTheFlood(2016)", "BirdBox(2018)", 
		"CaptainAmerica-TheFirstAvenger(2011)", "CaptainAmericaCivilWar(2016)", "Captain AmericaTheWinterSoldier (2014)", "Cargo(2018)", 
		"CatchMeIfYouCan(2002)", "CrazyRichAsians(2018)", "DoctorStrange(2016)", "Gattaca(1997)", "GuardiansoftheGalaxy(2014)", 
		"GuardiansoftheGalaxyVol.2(2017)", "IRobot(2004)", "IntheHeartoftheSea(2015)", "InglouriousBasterds(2009)", "Insidious(2010)", 
		"InsidiousChapter2(2013)", "InsidiousChapter3(2015)"};
	string director[25] = {"Ron Howard", "Peyton Reed", "Peyton Reed", "Mel Gibson", "Joss Whedon", "Anthony Russo",
		"Edgar Wright", "Fisher Stevens", "Susanne Bier", "Joe Johnston", "Joe Russo", "Joe Russo", "Yolanda Ramke", "Steven Spielberg", "Jon M. Chu",
		"Scott Derickson", "Andrew Niccol", "James Gunn", "James Gunn", "Alex Proyas", "Ron Howard", "Quentin Tarantino", "James Wan James", "Wan Leigh Whannell"};
	string producer[25] = {"Imagine Entertainment", "Kevin Feige", "Kevin Feige", "Mel Gibson", "Kevin Feige", "Kevin Feige", "Nira Park", "Leonardo DiCaprio", "Dylan Clark",
		"Kevin Feige", "Kevin Feige", "Kevin Feige", "Kristina Ceyton", "Steven Spielberg", "Nina Jacobson", "Kevin Feige", "Danny DeVito", "Kevin Feige", 
		"Kevin Feige", "Laurence Mark", "Brian Grazer", "Lawrence Bender", "Jason Blum", "Jason Blum", "Jason Blum"};
	string actors[25] = {"Russell Crowe", "Paul Rudd",
		"Paul Rudd, Evangeline Lilly", "Rudy Youngblood", "Robert Downey Jr., Chris Evans, Chris Hemsworth, Mark Ruffalo, Scarlett Johanson",
		"Robert Downey Jr., Chris Evans, Chris Hemsworth, Mark Ruffalo, Scarlett Johanson", "Ansel Elgort", "Leonardo DiCaprio", "Sandra Bullock",
		"Chris Evans", "Chris Evans", "Chris Evans", "Martin Freeman", "Leonardo DiCaprio", "Constance Wu", "Benedict Cumberbatch", "Vincent Freeman",
		"Chris Pratts, Zoe Zaldana, Dave Bautista, Vin Diesel, Bradley Cooper", "Chris Pratts, Zoe Zaldana, Dave Bautista, Vin Diesel, Bradley Cooper", 
		"Will Smith", "Chris Hemsworth", "Brad Pitt", "Patrick Wilson", "Patrick Wilson", "Lin Shaye"};
	string availability[25];
	string borrower[25];
	int dec;
	string key;
	int opt;
	string name;
	char yn = 'y';

	
	while(yn=='y'){
		x=1;
		cout << "Press (1) to search a movie or (2) to return a movie... \n";
		cin >> opt;
	
		switch(opt){
			case 1:
				search(title, director, producer, actors, availability, borrower);
				if(index!=25 && availability[index]!="borrowed"){
					cout << "\nThe dvd is available for rental, press [1] to procceed or [2] to cancel.." << endl;
					cin >> dec;
					if(dec==1){
						cout << "Enter your name: ";
						cin >> name;	
						availability[index] = "borrowed";
						borrower[index] = name;
						cout << "\nDVD successfully borrowed.";
					}
				}
				else if(x==1){
					cout << "\nThe dvd is borrowed by: " << borrower[index] << endl;
				}
			
				index = 25;
				break;
			case 2:
				cout << "Enter your name: ";
				cin >> name;
				cout << "\nEnter title of DVD to be returned: ";
				cin >> key;
				searchindex(title, key);
				if(index != 25 && availability[index]=="borrowed" && borrower[index]==name)
				{
					availability[index]="";
					borrower[index]="";
					cout << "\nDVD successfully returned!";
				}
				else if(borrower[index]!=name && x==1){
					cout << "\nInvalid request, borrower and return person does'nt match";
				}
				else if(x==1) {
					cout << "\nDVD is not borrowed";
				}
			default:
				break;
		}
		cout << "\nAnother transaction?(y/n)";
		cin >> yn;
	}
	return 0;
}

void search(string ti[], string di[], string pr[], string ac[], string av[], string bu[]){
	int num;
	string key;
	
	cout << "Search:  ";
	cin >> key;
	for(int i=0; i<=24; i++){
		int n = 0;
		if (ti[i]==key){
			cout << "Title: " << ti[i];
			cout << "\nDirector: " << di[i];
			cout << "\nProducer: " << pr[i];
			cout << "\nActors: " << ac[i];
			n = 1;
			index = i;
		}
		else if (index>24 && i==24){
			cout << "No match found" << endl;
			x=0;
		}
	}
}

void searchindex(string ttl[], string key2){
	for(int i=0; i<=24; i++){
		int n = 0;
		if (ttl[i]==key2){
			index = i;
		}
		else if (index>24 && i==24){
			cout << "No match found" << endl;
			x=0;
		}
	}
}